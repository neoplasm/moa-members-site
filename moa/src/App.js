import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import ListGroup from 'react-bootstrap/ListGroup';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberList: [],
      singleMember: {}
    };
  }

  componentDidMount() {
    fetch('https://moamembershipdraw.azurewebsites.net/api/members/')
      .then(res => res.json())
      .then(data => {
        this.setState({ memberList: data });
      })
      .catch(console.log);
  }
  render() {
    return (
      <div className="App">
        <div className="container">
          <center>
            <h1>Moa Member List  <img class="logo" src="./alterMoa2.PNG" alt="My_Logo"></img></h1>
          </center>
       

          <Button variant="primary" onClick={this.getMemberList.bind(this)}>
            Click to play
          </Button>{' '}
          <Alert variant="danger">
            The winner is {this.state.singleMember.id}  : {this.state.singleMember.firstName}{' '}
                  {this.state.singleMember.lastName}.
          </Alert>
          {this.state.memberList.map(member => (
            <div className="grid-container" key={member.id}>
              <ListGroup>
                <ListGroup.Item>
                  {member.membershipNumber} : {member.firstName}{' '}
                  {member.lastName}
                </ListGroup.Item>
              </ListGroup>
              {/* <div class="grid-item"></div> 
                <div class="grid-item"></div>  */}
            </div>
          ))}
        </div>
      </div>
    );
  }
  getMemberList() {
    fetch('https://moamembershipdraw.azurewebsites.net/api/members/draw')
      .then(res => res.json())
      .then(data => {
        this.setState({ singleMember: data });

      })
      .catch(console.log);
  }




  
}

export default App;
